import { useEffect, useState } from 'react';
import './Form.css';
import { doc, setDoc, onSnapshot } from "firebase/firestore";
import { todaysDate } from '../utils/todaysDate';

function Form(props) {
    const gameCode = localStorage.getItem('gameCode');
    const localTeamName = localStorage.getItem('teamName');
    const round = props.currentRound;
    const [header, setHeader] = useState([]);
    const [questions, setQuestions] = useState([]);
    const [roomCodeError, setRoomCodeError] = useState(false);

    async function saveAnswers(e) {
        e.preventDefault();
        try {
            await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', `round-${round}`, 'answers'), {
                1: {[localTeamName]: e.target[0].value.trim()},
                2: {[localTeamName]: e.target[1].value.trim()},
                3: {[localTeamName]: e.target[2].value.trim()},
                4: {[localTeamName]: e.target[3].value.trim()},
                5: {[localTeamName]: e.target[4].value.trim()},
                6: {[localTeamName]: e.target[5].value.trim()},
                7: {[localTeamName]: e.target[6].value.trim()}
            }, { merge: true })
        .then(() => {
            return saveTeamsAnswers(e.target);
        })
        } catch (e) {
            console.log('error saving to grading portal: ', e);
        }
    }

    async function saveTeamsAnswers(eTarget) {
        try {
            await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', 'teamNames', `${localTeamName}`), {
                [round]: {
                1: [eTarget[0].value, 0],
                2: [eTarget[1].value, 0],
                3: [eTarget[2].value, 0],
                4: [eTarget[3].value, 0],
                5: [eTarget[4].value, 0],
                6: [eTarget[5].value, 0],
                7: [eTarget[6].value, 0]
                }
            }, { merge: true })
        .then(() => {
            props.nextRound();
        })
        } catch (e) {
            console.log('error saving to teams records: ', e);
        }
    }

    const syncedLoad = (data) => {
        let loaded = [];
        const keys = Object.keys(data);
        {keys.map(key => {
            if (key === '8') {
                setHeader(<><h3>{data[key][0]}</h3><h6>{data[key][1]}</h6></>);
            } else if (data[key][1]) {
                const qDisplay = <div><label>{key}. {data[key][0]}</label></div>
                loaded.push(
                    <div key={key}>
                        {qDisplay}
                        <input name={key} type="text" />
                    </div>
                )
            }
        })}
        setQuestions(loaded);
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        async function fetchRound() {
            onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, `round-${round}`), (doc) => {
                if (doc.data()) {
                    const data = doc.data();
                    syncedLoad(data);
                } else {
                    setRoomCodeError(true);
                }
            });
        };
        fetchRound();
    }, []);

return (<>
    {header !== null ? <div>{header}</div> : <h3>Round {round}</h3>}
    {roomCodeError && <div className='error'>An error has occured. Could not fetch round {round}. Please see your host.</div>}
    <form className="round-form" onSubmit={saveAnswers}>
        {(questions.length > 0 && !roomCodeError) ? questions.sort((a, b) => parseFloat(a.key) - parseFloat(b.key)) : <div>Questions will be released shortly.</div>}
        {questions.length === 7 ? 
        <>
            <button type="submit" id='submit-round-button'>Submit</button>
        </> :
        <></>}
    </form>
</>);
}

export default Form;
