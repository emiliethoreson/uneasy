import React, { useState, useEffect } from 'react';
import { doc, setDoc, updateDoc, increment, onSnapshot } from "firebase/firestore";
import { todaysDate } from '../utils/todaysDate';
import './GameSignUp.css';

function EndScreen(props) {
    const gameCode = localStorage.getItem('gameCode');
    const localStorageTeamCode = localStorage.getItem('teamCode');
    const [teamCode] = useState(localStorageTeamCode);
    const [tiebreaker, setTiebreaker] = useState([]);

    const newGame = () => {
        localStorage.clear();
        window.location.reload();
    }

        async function saveTiebreaker(e) {
            e.preventDefault();
            const localTeamName = localStorage.getItem('teamName');
            try {
                await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', `round-7`, 'answers'), {
                    1: {[localTeamName]: e.target[0].value.trim()}
                }, { merge: true })
            .then(() => {
                setTiebreaker([<div>Tiebreaker has been submitted.</div>])
            })
            } catch (e) {
                console.log('error saving to grading portal: ', e);
            }
        }

    const syncedLoad = (data) => {
        let loaded = [];
        if (data[1][1]) {
            loaded.push(
            <form key={tiebreaker} onSubmit={saveTiebreaker}>
            <h6>Tiebreaker:</h6>
            <div><label>{data[1][0]}</label></div>
            <input name='tiebreaker-input' type="text" />
            <button type="submit" id='submit-tiebreaker-button'>Submit</button>
            </form>);
        }
        setTiebreaker(loaded);
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        async function fetchRound() {
            onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, `round-7`), (doc) => {
                if (doc.data()) {
                    const data = doc.data();
                    syncedLoad(data);
                }
            });
            if (teamCode) {
                console.log(teamCode);
                try {
                    await updateDoc(doc(props.db, 'tournamentPoints', `${teamCode}`), {
                        total: increment(props.cumulativeScore)
                    }, { merge: true })
                } catch (e) {
                    console.error('error saving total: ', e);
                }
            }
        };
        fetchRound();
    }, []);


    return (<div className='end-screen-skeleton'>
    <div className='end-message'>Everyone's scores will be announced soon! If you have a team code, your score has been saved.</div>
    {tiebreaker}
    <button onClick={newGame}>New game</button>
    </div>
    );
}

export default EndScreen;