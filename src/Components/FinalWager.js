import React, { useState } from 'react';
import { doc, setDoc } from "firebase/firestore";
import { todaysDate } from '../utils/todaysDate';
import './FinalWager.css';


function FinalWager(props) {
    const [wager, setWager] = useState();
    const gameCode = localStorage.getItem('gameCode');
    const localTeamName = localStorage.getItem('teamName');

    async function submitWager() {
        await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'bets'), {
            [localTeamName]: wager
        }, { merge: true })
        props.nextRound();
    }

    return (<div className='final-wager-skeleton'>
        <h3>Wait to hear the scores and category, then select a wager:</h3>
        <div className='button-grid'>
        <button key='0' onClick={() => {setWager(0)}} className={wager === 0 ? 'selected' : 'wager-button'}>0</button>
        <button key='1' onClick={() => {setWager(1)}} className={wager === 1 ? 'selected' : 'wager-button'}>1</button>
        <button key='2' onClick={() => {setWager(2)}} className={wager === 2 ? 'selected' : 'wager-button'}>2</button>
        <button key='3' onClick={() => {setWager(3)}} className={wager === 3 ? 'selected' : 'wager-button'}>3</button>
        <button key='4' onClick={() => {setWager(4)}} className={wager === 4 ? 'selected' : 'wager-button'}>4</button>
        <button key='5' onClick={() => {setWager(5)}} className={wager === 5 ? 'selected' : 'wager-button'}>5</button>

            {wager !== undefined && <button onClick={submitWager}>Wager {wager}</button>}
        </div>
    </div>
    );
}

export default FinalWager;
