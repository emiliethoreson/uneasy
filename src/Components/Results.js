import { useEffect, useState } from 'react';
import { doc, onSnapshot } from "firebase/firestore";
import { todaysDate } from '../utils/todaysDate';
import './Results.css';

function Results(props) {
    const round = parseInt(props.currentRound) !== NaN ? parseInt(props.currentRound) : props.currentRound;
    const gameCode = localStorage.getItem('gameCode');
    const localTeamName = localStorage.getItem('teamName');
    const [latestResults, setLatestResults] = useState([]);
    const [nextRound, setNextRound] = useState(false);

    const loadResults = (latestResults) => {
        let formatted = [];
        const roundResults = latestResults[round]
        for (const res in latestResults[round]) {
            formatted.push(<div key={'ans' + res + roundResults[res][1]} className='answer-row'>
                <div>{res}: {roundResults[res][0]}</div>
                {roundResults[res][1] > 0 && <div className='score-display'>+{roundResults[res][1]}</div>}
            </div>)
        }
        setLatestResults(formatted)
    }

    const listenForNextRound = (metaData) => {
        console.log(metaData.currentRound, props.currentRound)
        if (metaData.currentRound > props.currentRound) {
            setNextRound(true);
        }
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        async function fetchResults() {
            onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', 'teamNames', `${localTeamName}`), (doc) => {
                if (doc.data()) {
                    const data = doc.data();
                    loadResults(data);
                } else {
                    console.error('error fetching latest round results')
                }
            });
        };
        async function fetchNextRound() {
            onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, 'meta'), (doc) => {
                if (doc.data()) {
                    listenForNextRound(doc.data());
                } else {
                    console.error('cant get meta data to allow next round')
                }
            })
        }
        fetchResults();
        fetchNextRound();
    }, []);

    return (<>
        <h2>You answered:</h2>
        <div>
            <div className='score-table'>{latestResults}</div> 
            {nextRound && <button onClick={props.nextRound}>Next Round</button>}
        </div>
    </>);
}

export default Results;

