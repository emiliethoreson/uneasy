import React, { useEffect, useState } from 'react';
import { doc, setDoc, onSnapshot } from "firebase/firestore";
import { todaysDate } from '../utils/todaysDate';
import './FinalWager.css';

function Final(props) {
    const gameCode = localStorage.getItem('gameCode');
    const localTeamName = localStorage.getItem('teamName');
    const [questions, setQuestions] = useState([]);

    async function submitFinal(e) {
        e.preventDefault();
        try {
            await setDoc(doc(props.db, `${todaysDate}-${gameCode}`, 'teams', 'round-final', 'answers'), {
                1: {[localTeamName]: e.target[0].value.trim()}
            }, { merge: true })
            .then(() => {
                props.nextRound();
            });
        } catch (e) {
            console.log('error: ', e);
        }
    }



    const syncedLoad = (data) => {
        let loaded = [];
        console.log(data, )
            if (data[1][1]) {
                const qDisplay = <div><label>{data[1][0]}</label></div>
                loaded.push(
                    <div key='1'>
                        {qDisplay}
                        <input name='1' type="text" />
                    </div>
                )
            }
        setQuestions(loaded);
    }

    useEffect(() => {
        async function fetchRound() {
            onSnapshot(doc(props.db, `${todaysDate}-${gameCode}`, 'round-final'), (doc) => {
                if (doc.data()) {
                    const data = doc.data();
                    syncedLoad(data);
                } else {
                    console.log('error getting question...');
                }
            });
        };
        fetchRound();
    }, []);

    return (<div className='final-q-skeleton'>
    <div className='wager-header'>Final question:</div>
    <form onSubmit={submitFinal}>
    {questions.length > 0 ? <>{questions}</> : <div>Final question will be released shortly.</div>}
    {questions.length > 0 && <button type="submit" id='betting-round-button'>Submit</button>}
    </form>
    </div>
    );
}

export default Final;
