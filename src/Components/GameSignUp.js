import { useState } from 'react';
import './GameSignUp.css';
import { doc, getDoc, setDoc } from "firebase/firestore"; 
import { todaysDate } from '../utils/todaysDate';

function GameSignUp(props) {

    const [roomCodeError, setRoomCodeError] = useState(false);
    const [teamNameError, setTeamNameError] = useState(false);
    const [teamCodeError, setTeamCodeError] = useState(false);
    const [genericError, setGenericError] = useState(false);

    async function checkRoomCode(e) {
        e.preventDefault();
        const sanitizedName = sanitizeTeamName(e.target[0].value);
        const originalGameCode = e.target[1].value.trim();
        const allCapsGameCode = originalGameCode.toUpperCase();
        localStorage.setItem('teamName', sanitizedName);
        localStorage.setItem('gameCode', allCapsGameCode);
        if (e.target[2].value) {
            localStorage.setItem('teamCode', e.target[2].value);
        }
        const teamsRef = doc(props.db, `${todaysDate}-${allCapsGameCode}`, 'teams');
        const teamsDoc = await getDoc(teamsRef);
        if (teamsDoc.exists()) {
            checkTeamName(sanitizedName, teamsRef, teamsDoc);
        } else {
            console.error('incorrect room code')
            setRoomCodeError(true)
        }
    }

    const sanitizeTeamName = (incomingName) => {
        return incomingName.replace(/[.~*/\\\[\]']+/g, '');
    }

    async function checkTeamName(pendingTeamName, teamsDoc, teamsRef) {
        const teamAlreadyExists = teamsRef.data()[pendingTeamName];
       if (typeof teamAlreadyExists === 'number') {
            console.error('team name already taken')
            setTeamNameError(true)
       } else {
        checkTeamCode(pendingTeamName, teamsDoc)
       }
    }

    async function checkTeamCode(pendingTeamName, teamsDoc) {
        const teamCode = localStorage.getItem('teamCode');
        if (teamCode) {
            const docRef = doc(props.db, 'tournamentPoints', teamCode);
            const docSnap = await getDoc(docRef);
            if (docSnap.exists()) {
                createTeam(pendingTeamName, teamsDoc);
            } else {
                setTeamCodeError(true);
            }
        } else {
            createTeam(pendingTeamName, teamsDoc);
        }
    }

    async function createTeam(teamName, teamsDoc) {
        try {
            await setDoc(teamsDoc, {
            [teamName]: 0
        }, { merge: true })
        } catch (e) {
            setGenericError(true);
        }
        return props.nextRound();
    }

    return (<div className='sign-up-skeleton'>
        <h2>Let's play</h2>
        <form className="form" onSubmit={checkRoomCode}>
            <div className="sign-up-form">
                <div>Team name:</div>
                <div><input name="Team Name" type="text" required /></div>
                <div>Game code:</div>
                <div><input name="Room Code" type="text" className='room-code-input' required /></div>
                <div>Optional team code:</div>
                <div><input name="Team Code" type="number"></input></div>
                <button type="submit" className='sign-up-button'>Sign up</button>
                {roomCodeError && <span className="sign-up-error">We couldn't find a game under this room code. Please check with your host.</span>}
                {teamNameError && <span className="sign-up-error">A team with that name already exists. Please choose a different name.</span>}
                {teamCodeError && <span className="sign-up-error">There's no team registered under this number. Please check the email you received.</span>}
                {genericError && <span className="sign-up-error">We could not log you into the game. Please check with your host.</span>}
            </div>
        </form>
    </div>
    );
}

export default GameSignUp;