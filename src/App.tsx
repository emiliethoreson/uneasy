import './App.css';
import GameSignUp from './Components/GameSignUp.js';
import Form from './Components/Form.js';
import Results from './Components/Results.js';
import FinalWager from './Components/FinalWager.js';
import Final from './Components/Final.js';
import EndScreen from './Components/EndScreen.js';
import { useCallback, useEffect, useState } from 'react';
import { initializeApp } from 'firebase/app';
import { getFirestore, doc, getDoc } from 'firebase/firestore';
import { todaysDate } from './utils/todaysDate';

function App() {
  const round = localStorage.getItem('round');
  const [currentRound, setCurrentRound] = useState(round ? round : 'signup');
  const [cumulativeScore, setCumulativeScore] = useState(0);

  const roundOrder = ['signup', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '5.5', '6', '6.5', 'final-wager', 'final', 'end', 'tiebreaker'];

  const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_API,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGE_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_ID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
  };

  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);

  const nextRound = useCallback(() => {
    const currentRoundIndex = roundOrder.indexOf(currentRound);
    const nextRound = roundOrder[currentRoundIndex + 1];
    setCurrentRound(nextRound);
    localStorage.setItem('round', nextRound);
  }, [currentRound]);

  const loadGame = () => {
    let newRound;
    switch (currentRound) {
      case 'signup':
        newRound = <GameSignUp nextRound={nextRound} db={db} />;
        break;
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
        fetchCurrentScore();
        newRound = <Form currentRound={currentRound} nextRound={nextRound} db={db} />;
        break;
      case '1.5':
      case '2.5':
      case '3.5':
      case '4.5':
      case '5.5':
      case '6.5':
        newRound = <Results currentRound={currentRound} nextRound={nextRound} db={db} />;
        break;
      case 'final-wager':
        newRound = <FinalWager db={db} nextRound={nextRound} />;
        break;
      case 'final':
        newRound = <Final db={db} nextRound={nextRound} />;
        break;
      case 'end':
        fetchCurrentScore();
        newRound = <EndScreen db={db} cumulativeScore={cumulativeScore} />;
        break;
      default:
        newRound = <GameSignUp nextRound={nextRound} db={db} />;
        break;
    }
    return newRound;
  };

  async function fetchCurrentScore() {
    if (roundOrder.indexOf(currentRound) < 2) { return; }
    const gameCode = localStorage.getItem('gameCode');
    const localTeamName = localStorage.getItem('teamName');
    if (gameCode && localTeamName) {
      const docRef = doc(db, `${todaysDate}-${gameCode}`, 'teams');
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        const currentTotal = docSnap.data()[localTeamName];
        setCumulativeScore(currentTotal);
      }
    }
  }

  const adminReset = () => {
    let codePrompt = window.prompt('enter password for reset:', '');
    if (codePrompt === 'hardreset' || codePrompt === 'Hardreset') {
      localStorage.clear();
      window.location.reload();
    }
    if (codePrompt === 'roomff' || codePrompt === 'Roomff') {
      let room = window.prompt('enter level:', '')!;
      localStorage.setItem('round', room);
      setCurrentRound(room);
    }
  };

  useEffect(() => {
    const gameCode = localStorage.getItem('gameCode');
    if (gameCode) {
      const checkCookies = async function () {
        const docRef = doc(db, `${todaysDate}-${gameCode}`, 'teams');
        const docSnap = await getDoc(docRef);
        const reloadingRound = localStorage.getItem('round');
        if (docSnap.exists() && reloadingRound) {
          setCurrentRound(reloadingRound);
          loadGame();
        } else {
          localStorage.clear();
          window.location.reload();
        }
      };
      checkCookies();
    }
  }, [db, loadGame]);

  return (
    <div className="App">
      <header className="App-header">        
      </header>
      <div className='content-skeleton'>
        {loadGame()}
      </div>
      <div className='footer'>
        {roundOrder.indexOf(currentRound) > 2 && <div className='total-display'>Total: {cumulativeScore}</div>}
        <div className='footer-label'>Uneasy Events player answer portal v2.0.4</div>
      <button className='hard-reset' onClick={adminReset}>x</button>
      </div>
    </div>
  );
}

export default App;
