module.exports = {
    plugins: [
        new webpack.DefinePlugin({
            'process.env.REACT_APP_FIREBASE_API': JSON.stringify(process.env.REACT_APP_FIREBASE_API),
            'process.env.REACT_APP_FIREBASE_DOMAIN': JSON.stringify(process.env.REACT_APP_FIREBASE_DOMAIN),
            'process.env.REACT_APP_FIREBASE_PROJECT': JSON.stringify(process.env.REACT_APP_FIREBASE_PROJECT),
            'process.env.REACT_APP_STORAGE_BUCKET': JSON.stringify(process.env.REACT_APP_STORAGE_BUCKET),
            'process.env.REACT_APP_MESSAGE_SENDER_ID': JSON.stringify(process.env.REACT_APP_MESSAGE_SENDER_ID),
            'process.env.REACT_APP_FIREBASE_ID': JSON.stringify(process.env.REACT_APP_FIREBASE_ID),
            'process.env.REACT_APP_FIREBASE_MEASUREMENT_ID': JSON.stringify(process.env.REACT_APP_FIREBASE_MEASUREMENT_ID)
        })
    ],
}